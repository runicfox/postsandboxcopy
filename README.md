# README #

An extensible configuration-driven design for a post-sandbox copy solution.

### What is this repository for? ###

* Apply different environment-based approaches for the post-sandbox copy script without requiring multiple implementations.
* v.0.1

### How do I get set up? ###

* Fork or clone this repository.

### Contribution guidelines ###

* Writing tests
	* All Apex code is required to have corresponding unit tests with at least 99% coverage.
* Code review
	* All submmitted code will be required to pass assessment from the Apex PMD and SCA.
* Other guidelines
	* Before performing any work for a pull request, it is recommended that you first check with the repo's authors to prevent duplication or work, or having work conflict with current or future (roadmapped) development.

### Who do I talk to? ###

* Matt Higel: matthew.higel@gmail.com